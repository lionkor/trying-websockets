#include <websocketpp/config/asio_no_tls.hpp>

#include <websocketpp/common/connection_hdl.hpp>
#include <websocketpp/server.hpp>

#include <functional>
#include <sqlite3.h>
#include <string_view>

#include "RawPtr.h"

using WSServer = websocketpp::server<websocketpp::config::asio>;

static RawPtr<sqlite3> db;

void on_message(WSServer* s, websocketpp::connection_hdl hdl, WSServer::message_ptr msg) {
    s->send(hdl, "OK.", msg->get_opcode());
}

struct X {
    int a;
};

int run_server() {
    WSServer server;
    server.init_asio();
    server.set_message_handler(std::bind(&on_message, &server, std::placeholders::_1, std::placeholders::_2));
    server.listen(6699);
    server.start_accept();
    server.run();

    return EXIT_SUCCESS;
}

int run_client() {
    return EXIT_SUCCESS;
}

int main(int argc, char** argv) {
    if (argc < 2) {
        std::cerr << "missing argument: 'client' or 'server'." << std::endl;
        return EXIT_FAILURE;
    }
    if (std::string_view(argv[1]) == "client") {
        return run_client();
    } else if (std::string_view(argv[1]) == "server") {
        return run_server();
    } else {
        std::cerr << "invalid argument: expected either 'client' or 'server'." << std::endl;
    }
}
